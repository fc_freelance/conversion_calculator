module.exports = function () {
    gulp.watch(["app/js/**/*.js", "app/js/**/*.jsx"], ['compile']);
    gulp.watch("app/less/**/*.less", ['css']);
};