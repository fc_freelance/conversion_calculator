module.exports = function () {
    gulp.src("app/app.js")
        .pipe(uglify())
        .pipe(rename('app.min.js'))
        .pipe(gulp.dest("build"));
    gulp.src("app/app.min.css")
        .pipe(gulp.dest("build"));
    gulp.src(['app/assets/**/*'])
        .pipe(gulp.dest('build/assets'));
    return gulp.src("app/index.html")
        .pipe(replace(/app\.js/g, 'app.min.js'))
        .pipe(gulp.dest("build"));
};