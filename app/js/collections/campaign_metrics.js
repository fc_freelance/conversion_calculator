import Backbone from 'backbone'

import {default as CampaignMetric} from '../models/campaign_metric'

export default Backbone.Collection.extend({
    model: CampaignMetric
})
