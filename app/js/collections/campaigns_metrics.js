import Backbone from 'backbone'

import {default as CampaignMetrics} from '../models/campaign_metrics'

export default Backbone.Collection.extend({
    model: CampaignMetrics,

    sync: (method, model, options = {}) => {
        options.dataType = options.dataType || 'xml';

        let now = new Date(),
            from = (options.from && new Date(options.from) || now).toJSON().slice(0, 10).split('-').join(''),
            // TODO: 2 years in the past enough?
            prev = new Date(now.setFullYear(now.getFullYear() - 2)),
            to = (options.to && new Date(options.to) || prev).toJSON().slice(0, 10).split('-').join(''),
            models_count = model.length;

        if (!model.length) {
            return options.success(model, "success", options);
        }

        model.models.forEach(entry => {
            entry.sync(method, entry, {
                from: from,
                to: to,
                success: (mdl, res, opts) => {
                    entry.set(entry.parse(mdl, opts));
                    if (!(--models_count)) {
                        options.success(model, "success", options)
                    }
                },
                error: (mdl, res, opts) => {
                    if (!(--models_count)) {
                        options.error(model, "error", options)
                    }
                }
            });
        });
    }
})
