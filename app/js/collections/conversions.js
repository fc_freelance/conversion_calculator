import Backbone from 'backbone'

import {default as Conversion} from '../models/conversion'

export default Backbone.Collection.extend({
    model: Conversion,
    baseUrl: '<the endpoint here, either from input field or hardcoded if public>',

    sync: (method, model, options = {}) => {
        options.dataType = options.dataType || 'xml';
        let limit = 1000,
            now = Math.round(new Date().getTime()/1000) + 24 * 60 * 60,
            prev = now - 30 * 24 * 60 * 60,
            start = options.start && Math.round(options.start / 1000) || now,
            end = options.end && Math.round(options.end / 1000) || prev,
            store_id = options.store_id || '<some default store id>';
        model.url = model.baseUrl + '?store_id=' + store_id + '&start=' + start + '&end=' + end + '&limit=' + limit;
        Backbone.Collection.prototype.sync.call(model, method, model, options)
    },

    parse: (data) => {
        let c = $(data).find('activity').map((i, entry) => ({
            id: $(entry).find('id').text(),
            date: parseInt($(entry).find('date').text(), 10) * 1000,
            storeName: $(entry).find('storename').text(),
            campaignId: parseInt($(entry).find('campaignid').text(), 10),
            campaignName: $(entry).find('campaignname').text(),
            browser: $(entry).find('browser').text(),
            os: $(entry).find('os').text(),
            device: $(entry).find('device').text(),
            wasConversion: $(entry).find('conversion').text() === 'yes',
            wasCampaign: $(entry).find('campaign').text() === 'yes',
            wasInteracted: $(entry).find('interaction').text() === 'yes',
            location: decodeURIComponent($(entry).find('location').text().replace(/\+/g, ' '))
        }));
        return c.get();
    }
})
