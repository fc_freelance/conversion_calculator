import Backbone from 'backbone'

import {default as Conversions} from './collections/conversions'
import {default as CampaignMetrics} from './models/campaign_metrics'
import {default as CampaignsMetrics} from './collections/campaigns_metrics'
import {default as MainView} from './views/main.jsx'

export default Backbone.Router.extend({
    collection: null,
    campaignsMetricsCollection: null,
    rootEl: null,
    filters: null,
    views: {
        main: null
    },

    routes: {
        '(/)': 'default'
    },

    initialize(options = {}) {
        this.collection = options.collection || new Conversions();
        this.campaignsMetricsCollection = options.campaignsMetricsCollection || new CampaignsMetrics();
        this.rootEl = options.rootEl || '.main-wrapper';
        this.filters = options.filters || {};

        this.views.main = new MainView({
            router: this,
            collection: this.collection,
            campaignsMetricsCollection: this.campaignsMetricsCollection,
            el: this.rootEl
        });
    },

    start() {
        this.views.main.render();
        if (!Backbone.history.started) {
            Backbone.history.start({pushState: true});
        }
    },

    default(qs) {

        if (!this.views.main) {
            return console.error('Needed MainView but not available!');
        }

        let now = new Date().getTime() + 24 * 60 * 60 * 1000,
            prev = now - 30 * 24 * 60 * 60 * 1000,
            match = /chart=([^&$]+)?&store_id=([^&$]+)?&start=([^&$]+)?&end=([^&$]+)?/.exec(qs),
            chart = match && match[1] || 'line',
            store_id = match && match[2] || '<some default store id>',
            start = match && match[3] && parseInt(match[3], 10) || now,
            end = match && match[4] && parseInt(match[4], 10) || prev,
            // TODO: Add option to specify number of records, as 'limit' parameter
            loading = false;

        if (this.filters.start !== start || this.filters.end !== end || this.filters.store_id !== store_id) {

            if (this.collection.length > 0 && this.filters.store_id === store_id) {

                let min_date = Infinity,
                    max_date = 0,
                    del_ids = [];

                this.collection.models.forEach(entry => {
                    let t = entry.get('date');
                    if (t > start || t < end) {
                        del_ids.push(entry.get('id'))
                    } else {
                        min_date = (t < min_date) ? t : min_date;
                        max_date = (t > max_date) ? t : max_date
                    }
                });

                if (del_ids.length > 0) {
                    this.collection.remove(del_ids);
                    this.collection.trigger('reset')
                }

                if (min_date > end && min_date > this.filters.end) {

                    // Get everything
                    this.loadCollectionRecursively({
                        store_id: store_id,
                        start: min_date,
                        end: end
                    });
                }

                if (max_date < start && max_date < this.filters.start) {

                    // Get everything
                    this.loadCollectionRecursively({
                        store_id: store_id,
                        start: start,
                        end: max_date
                    });
                }

            } else {

                // Get everything
                this.loadCollectionRecursively({
                    // TODO: necessary?
                    // reset: true,
                    store_id: store_id,
                    start: start,
                    end: end
                });

            }

            this.filters.start = start;
            this.filters.end = end;
            this.filters.store_id = store_id;
            loading = true;
            this.views.main.update({loading: loading});
        }

        if (this.filters.chart !== chart) {
            this.filters.chart = chart;
            this.views.main.update({filters: this.filters, loading: loading});
        }
    },

    loadCollectionRecursively(options = {}) {
        let now = new Date().getTime() + 24 * 60 * 60 * 1000,
            prev = now - 30 * 24 * 60 * 60 * 1000;

        this.collection.fetch({
            reset: false,//TODO please review this !!options.reset,
            //add: !options.reset,
            remove: false,
            store_id: options.store_id,
            start: options.start || now,
            end: options.end || prev,

            success: (col, res, opts) => {
                let next = parseInt($(res).find('next').text(), 10) * 1000;
                if (next && next > options.end) {
                    this.loadCollectionRecursively({
                        start: next,
                        end: options.end
                    });
                // Got all conversions, now get the campaigns active date ranges
                } else {
                    this.loadCampaignsMetricsCollections();
                }
            }
        });
    },

    loadCampaignsMetricsCollections(options = {}) {
        let campaignIds = _.uniq(this.collection.pluck('campaignId')).map(id => (new CampaignMetrics({id: id})));
        this.campaignsMetricsCollection.reset(campaignIds);
        this.campaignsMetricsCollection.fetch({
            from: this.filters.end,
            to: this.filters.start,
            store_id: this.filters.store_id,
            remove: false,
            add: false
        });
    }
})
