import 'react'

export default class Loader extends React.Component {

    constructor(props) {
        super(props);
        this.state = {show: props.show};
    }

    componentWillMount() {
        //console.log('Content Will mount ...', this);
    }

    componentDidMount() {
        //console.log('Content Mounted!', this);
    }

    componentWillUnmount() {
        console.log('Loader Will unmount ...', this);
    }

    componentWillReceiveProps(nextProps) {
        console.log('Loader Will receive props ...', this, nextProps);
        this.state.show = nextProps.show;
    }

    shouldComponentUpdate(nextProps, nextState) {
        console.log('Should Loader update?', nextProps, nextState);
        return this.props.show !== nextProps.show;
    }

    componentWillUpdate(nextProps, nextState) {
        console.log('Loader Will update ...', this, nextProps, nextState);
    }

    componentDidUpdate(prevProps, prevState) {
        console.log('Loader Updated!!', this, prevProps, prevState);
    }

    render() {
        console.log('Rendering Loader!!!');
        var className = "fc-loader " + (this.state.show ? "active" : "");
        return (
            <div className={className}></div>
        )
    }
}
