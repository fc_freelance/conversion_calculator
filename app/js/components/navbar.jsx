import 'react'

export default class Navbar extends React.Component {

    constructor(props) {
        super(props);
        this.onStoreIdChange = this.onStoreIdChange.bind(this);
        this.onStartChange = this.onStartChange.bind(this);
        this.onEndChange = this.onEndChange.bind(this);
        this.onChartChange = this.onChartChange.bind(this);
    }

    componentWillReceiveProps(nextProps) {
        console.log('Navbar Will receive props ...', this, nextProps);
    }

    shouldComponentUpdate(nextProps, nextState) {
        console.log('Should Navbar update?', nextProps, nextState);
        let b = nextProps.filters &&
            (nextProps.filters.chart && (this.props.filters.chart !== nextProps.filters.chart) ||
            nextProps.filters.store_id && (this.props.filters.store_id !== nextProps.filters.store_id) ||
            nextProps.filters.start && (this.props.filters.start !== nextProps.filters.start) ||
            nextProps.filters.end && (this.props.filters.end !== nextProps.filters.end));
        return !!b;
    }

    componentWillUpdate(nextProps, nextState) {
        console.log('Navbar Will update ...', this, nextProps, nextState);
    }

    componentDidUpdate(prevProps, prevState) {
        console.log('Navbar Updated!!', this, prevProps, prevState);
    }

    onChartChange(evt) {
        console.log('Changed chart', evt);
        this.props.router.navigate(
            '?chart=' + evt.target.getAttribute('target') +
            '&store_id=' + (this.props.filters.store_id || '') +
            '&start=' + (this.props.filters.start || '') +
            '&end=' + (this.props.filters.end || ''),
            {trigger: true}
        );
    }

    onStoreIdChange(evt) {
        console.log('Changed store id', evt);
        this.props.router.navigate(
            '?chart=' + (this.props.filters.chart || 'line') +
            '&store_id=' + evt.target.value +
            '&start=' + (this.props.filters.start || '') +
            '&end=' + (this.props.filters.end || ''),
            {trigger: true}
        );
    }

    onStartChange(evt) {
        console.log('Changed start date', evt);
        this.props.router.navigate(
            '?chart=' + (this.props.filters.chart || 'line') +
            '&store_id=' + (this.props.filters.store_id || '') +
            '&start=' + new Date(evt.target.value).getTime() +
            '&end=' + (this.props.filters.end || ''),
            {trigger: true}
        );
    }

    onEndChange(evt) {
        console.log('Changed end date', evt);
        this.props.router.navigate(
            '?chart=' + (this.props.filters.chart || 'line') +
            '&store_id=' + (this.props.filters.store_id || '') +
            '&start=' + (this.props.filters.start || '') +
            '&end=' + new Date(evt.target.value).getTime(),
            {trigger: true}
        );
    }

    render() {
        console.log('Rendering Navbar!!!');
        let store_id = this.props.filters.store_id || '',
            start = this.props.filters.start ? new Date(this.props.filters.start).toISOString().substring(0, 10) : '',
            end = this.props.filters.end ? new Date(this.props.filters.end).toISOString().substring(0, 10) : '';
        return (
            <nav className="navbar navbar-inverse">
                <div className="container-fluid">
                    <div className="navbar-header">
                        <button type="button" className="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                            <span className="sr-only">Toggle navigation</span>
                            <span className="icon-bar" />
                            <span className="icon-bar" />
                            <span className="icon-bar" />
                        </button>
                        <a className="navbar-brand" href="/">
                            <img src="/assets/img/logo.jpg" alt="Frontcoder"/>
                        </a>
                    </div>
                    <div className="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                        <ul className="nav navbar-nav">
                            <li className={this.props.filters.chart === 'line' ? 'active' : ''}>
                                <a href="javascript:void(0)" onClick={this.onChartChange} target="line">Linear</a>
                            </li>
                        </ul>
                        <form className="navbar-form navbar-right" role="search">
                            <div className="form-group">
                                <label htmlFor="store_id">Store Id</label> &nbsp;
                                <input type="text" id="store_id" name="store_id" className="form-control" placeholder="Store Id" value={store_id} onChange={this.onStoreIdChange} />
                            </div>
                            &nbsp; &nbsp; &nbsp; &nbsp;
                            <div className="form-group">
                                <label htmlFor="end">Start</label> &nbsp;
                                <input type="date" id="end" name="end" className="form-control" placeholder="Start Date" value={end} onChange={this.onEndChange} />
                            </div>
                            &nbsp; &nbsp; &nbsp; &nbsp;
                            <div className="form-group">
                                <label htmlFor="start">End</label> &nbsp;
                                <input type="date" id="start" name="start" className="form-control" placeholder="End Date" value={start} onChange={this.onStartChange} />
                            </div>
                        </form>
                    </div>
                </div>
            </nav>
        )
    }
}
