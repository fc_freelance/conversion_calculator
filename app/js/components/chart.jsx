import 'react'
import 'd3'
import * as ss from 'simple-statistics'

/**
 * Filters model
 *
 * @type {*[]}
 */
const filterFields = [
    {name: 'Store', key: 'storeName'},
    {name: 'Campaign', key: 'campaignName'},
    {name: 'With Campaign', key: 'wasCampaign', handler: d => d ? 'Yes' : 'No'},
    {name: 'Interacted', key: 'wasInteracted', handler: d => d ? 'Yes' : 'No'},
    {name: 'Browser', key: 'browser'},
    {name: 'OS', key: 'os'},
    {name: 'Device', key: 'device'},
    {name: 'Country', key: 'location', handler: d => d.split(/ ?, ?/)[1]},
    {name: 'City', key: 'location', handler: d => d.split(/ ?, ?/)[0]}
];

/**
 * Exports component as default
 */
export default class Chart extends React.Component {

    /**
     * @param props
     */
    constructor(props) {
        super(props);
        this._chart = null;
        this._filters = {};
        this._data = [];
        this._metricsData = [];
    }

    /**
     * @returns *
     */
    get chart() {
        return this._chart
    }

    /**
     * @param chart
     */
    set chart(chart) {
        this._chart = chart
    }

    /**
     * Called whenever component attempts to mount
     */
    componentWillMount() {
        //console.log('Chart Will mount ...', this);
    }

    /**
     * Called whenever component was mounted
     */
    componentDidMount() {
        console.log('Chart Mounted!', this);
        this.renderChart();
    }

    /**
     * Called whenever component attempts to unmount
     */
    componentWillUnmount() {
        console.log('Chart Will unmount ...', this);
    }

    /**
     * Called whenever component is going to receive new props
     *
     * @param nextProps
     */
    componentWillReceiveProps(nextProps) {
        console.log('Chart Will receive props ...', this, nextProps);
        this._data = this.filterData(nextProps.data);
        this._metricsData = nextProps.metricsData;
        this.computeFilters();
    }

    /**
     * Called whenever component attempts to update so it's possible to discard update just returning false
     *
     * @param nextProps
     * @param nextState
     * @returns {*|boolean}
     */
    shouldComponentUpdate(nextProps, nextState) {
        console.log('Should Chart update?', nextProps, nextState);
        // FIXME: Maybe we should trigger here d3 update and return false??
        let b = nextProps.data && JSON.stringify(nextProps.data) !== JSON.stringify(this.props.data) ||
            nextProps.metricsData && JSON.stringify(nextProps.metricsData) !== JSON.stringify(this.props.metricsData) ||
            (nextProps.filters && (nextProps.filters.chart && (this.props.filters.chart !== nextProps.filters.chart) ||
            nextProps.filters.start && (this.props.filters.start !== nextProps.filters.start) ||
            nextProps.filters.end && (this.props.filters.end !== nextProps.filters.end)));
        return !!b
    }

    /**
     * Called whenever component attempts to update
     *
     * @param nextProps
     * @param nextState
     */
    componentWillUpdate(nextProps, nextState) {
        console.log('Chart Will update ...', this, nextProps, nextState);
    }

    /**
     * Called whenever component was updated
     *
     * @param prevProps
     * @param prevState
     */
    componentDidUpdate(prevProps, prevState) {
        console.log('Chart Updated!!', this, prevProps, prevState);
        this.updateChart(this.getChartData(this._data, this._metricsData))
    }

    /**
     * Renders HTML for the component
     *
     * @returns {XML}
     */
    render() {
        console.log('Rendering Chart!!!');
        return (
            <div>
                <div className="row">
                    <div className="inputs col-sm-2">
                        <button className="btn btn-success oe-reset-filters" onClick={this.resetFilters.bind(this)}>Reset Filters</button>
                        {Object.keys(this._filters).map(k =>
                            <div key={k} className="form-group">
                                <label htmlFor={k}>{k}</label>
                                <select name={k} id={k} className="form-control" value={this._filters[k].value} onChange={this.onFilterChange.bind(this)}>
                                    {this._filters[k].options.map(f =>
                                        <option key={f} value={f}>{f}</option>
                                    )}
                                </select>
                            </div>
                        )}
                    </div>
                    <div className="chart-wrapper col-sm-10">
                        <div className="chart"></div>
                    </div>
                </div>
                <div className="chart-tooltip hidden">
                    <p>
                        <span className="date">&nbsp;</span>
                        <span className="value">&nbsp;</span>
                    </p>
                </div>
            </div>
        )
    }

    /**
     * Resets the filters and forces rendering
     */
    resetFilters() {
        this._filters = {};
        this._data = this.filterData(this.props.data);
        this.computeFilters();
        this.forceUpdate()
    }

    /**
     * Recreates the filters with existing data
     */
    computeFilters() {

        // Generate general computed dataset
        this._filters = this._data.reduce((acc, d) =>

            filterFields.reduce((acc2, f) => {

                if (!acc2[f.name] || !acc2[f.name].value || acc2[f.name].value === 'All') {

                    let rawVal = d[f.key],
                        val = (rawVal || [0, false].indexOf(rawVal) !== -1) && (f.handler ? f.handler(rawVal) : rawVal);

                    if (!val || ['not found', 'null'].indexOf(val) !== -1) return acc2;

                    acc2[f.name] = acc2[f.name] || {options: [], value: null};

                    if (acc2[f.name].options.indexOf(val) === -1) {
                        acc2[f.name].options.push(val);
                    }
                }

                return acc2;

            }, acc)

        , this._filters);

        // Add 'All' options and sort fields
        Object.keys(this._filters).forEach(k => {
            if (this._filters[k].options.length && this._filters[k].options.indexOf('All') === -1) {
                this._filters[k].options.push('All');
            }
            this._filters[k].options.sort((a, b) => (b === 'All') ? 1 : ((a === 'All') ? -1 : (b.toUpperCase() < a.toUpperCase() ? 1 : b.toUpperCase() > a.toUpperCase() ? -1 : 0)));
            if (!this._filters[k].value) {
                this._filters[k].value = this._filters[k].options[0];
            }
        })
    }

    /**
     * Recomputes filters and forces rendering
     *
     * @param evt
     */
    onFilterChange(evt = window.event) {
        this._filters[evt.target.name].value = evt.target.value;
        this._data = this.filterData(this.props.data);
        this.computeFilters();
        this.forceUpdate()
    }

    /**
     * Filters data
     *
     * @param data
     * @returns {*}
     */
    filterData(data) {

        return data.filter(d =>
            filterFields.reduce((b, f) =>
                b && (!this._filters || !this._filters[f.name] || !this._filters[f.name].value ||
                    this._filters[f.name].value === 'All' || this._filters[f.name].value === (f.handler ? f.handler(d[f.key]) : d[f.key]))
            , true)
        );
    }

    /**
     * Gets proper datasets for a chart given an incoming raw data
     *
     * @param data
     * @param metricsData
     * @returns {*[]}
     */
    getChartData(data, metricsData) {

        // Skeleton object for new daily leads
        let dailySkeleton = {
            total: 0,
            converted: 0
        };

        // Generate general computed dataset
        let leads = data.reduce((acc, d) => {

            // Get the day from element
            let day = new Date(new Date(d.date).toDateString()).getTime();

            // Init record set if not yet
            if (!acc[day]) acc[day] = $.extend(true, {}, dailySkeleton);

            // Increment total counter
            acc[day].total++;

            // Check if it was conversion, and increase counter if so
            if (d.wasConversion) {
                acc[day].converted++;
            }

            // Return accumulated structure
            return acc;
        }, {});

        // Init variable to start date
        let day = this.props.filters.end;

        // Loop day by day until we reach end date
        while (day <= this.props.filters.start) {

            // Fix day date for discrepances ....
            let fixedDay = new Date(new Date(day).toDateString()).getTime();

            // Check if we got that day from previous data loop, otherwise init it with zeroed values
            leads[fixedDay] = leads[fixedDay] || $.extend(true, {}, dailySkeleton);

            // Increment by one day
            day += 24 * 60 * 60 * 1000;
        }

        // Let's sort the keys here
        let keys = Object.keys(leads).sort((a, b) => a - b);

        // Generate dataset for global performance with provided filtered dataset
        let globalPerformance = keys.map(function(key) {
            return [parseInt(key), (leads[key].converted / leads[key].total) * 100 || 0]
        });

        // Generate dataset for global performance when a campaign was not visible
        //let globalPerformanceWithoutCampaign = keys.map(function(key) {
        //    var convIdx = leads[key].no_campaign.converted / leads[key].no_campaign.total || 0,
        //        estimatedWC = Math.round(leads[key].campaign.total * convIdx),
        //        total_converted = leads[key].no_campaign.converted + estimatedWC;
        //    return [parseInt(key), (total_converted / leads[key].total) * 100 || 0]
        //});

        // console.log('Global Performance', globalPerformance);

        // Now prepare campaign limits if got metrics data

        let filteredCampaignIds = data.reduce((acc, d) => acc.concat(acc.indexOf(d.campaignId) === -1 ? [d.campaignId] : []), []);

        let campaignDateRanges = filteredCampaignIds.reduce((acc, id) => {

            let dates = metricsData && metricsData.filter(metric => metric.id === id).reduce((acc2, m) => {
                    let facts = m.facts && m.facts.toJSON();
                    if (!facts) return acc2;
                    return acc2.concat(facts.reduce((acc3, fact) => fact.loads ? acc3.concat(fact.period) : acc3, []))
                }, []);

            return dates ? acc.concat(dates) : acc;

        }, []).sort((a, b) => a.getTime() - b.getTime());

        console.log(campaignDateRanges);

        let activeDatesRange = {from: campaignDateRanges[0], to: campaignDateRanges.pop()};

        // Return grouped datasets so they can render together in a chart
        return [
            { title: 'Global Performance', data: globalPerformance, metricsData: { activeDatesRange: activeDatesRange} }
        ];
    }

    /**
     * Creates chart main components, without any data yet
     *
     * @returns {*}
     */
    renderChart() {

        // Chart dimensions
        let margin = {top: 20, right: 20, bottom: 30, left: 50},
            containerEl = $('.chart'),
            width = containerEl.width() - margin.left - margin.right,
            height = containerEl.height() - margin.top - margin.bottom;

        // Main containers for chart
        let svg = d3.select(".chart").append("svg")
            .attr("width", width + margin.left + margin.right)
            .attr("height", height + margin.top + margin.bottom)
            .append("g")
            .attr("width", width)
            .attr("height", height)
            .attr("transform", "translate(" + margin.left + "," + margin.top + ")");

        // X scale
        let x = d3.time.scale()
            .range([0, width]);

        // Y scale
        let y = d3.scale.linear()
            .range([height, 0]);

        // X Axis
        let xAxis = d3.svg.axis()
            .scale(x)
            .orient("bottom");

        // Y Axis
        let yAxis = d3.svg.axis()
            .scale(y)
            .tickFormat(d => d * 100 + '%')
            .orient("left");

        // Define Area calculator
        let area = d3.svg.area()
            .x(function(d) { return x(new Date(d[0])); })
            .y0(height)
            .y1(function(d) { return y(+d[1]); })
            .interpolate("monotone");

        // Define line calculator
        let line = d3.svg.line()
            .x(function(d) { return x(new Date(d[0])); })
            .y(function(d) { return y(+d[1]); })
            .interpolate("monotone");

        // Append X axes
        svg.append("g")
            .attr("class", "x axis")
            .attr("transform", "translate(0," + height +  ")");

        // Append Y axes
        let yAxisEl = svg.append("g")
            .attr("class", "y axis")
            .call(yAxis);

        // Set horizontal lines on Y Axis
        yAxisEl.selectAll("g")
            .attr("class", "minor");

        // Set horizontal lines on Y Axis
        yAxisEl.selectAll("line")
            .attr("x1", -30)
            .attr("x2", width);

        // Set text position on Y Axis
        yAxisEl.selectAll("text")
            .attr("x", 4)
            .attr("dy", -4);

        // Define brush
        let brush = d3.svg.brush()
            .x(x)
            .on("brushend", () => this.updateChart(this.chart.datasets));

        // Append brush to chart
        svg.append("g")
            .attr("class", "x brush")
            .call(brush)
            .selectAll("rect")
            .attr("y", -6)
            .attr("height", height + 7);

        // Store chart elements in instance property
        this.chart = {
            el: svg,
            dims: {
                w: width,
                h: height,
                margin: margin
            },
            domain: {
                x: x,
                y: y
            },
            axis: {
                x: xAxis,
                y: yAxis
            },
            line: line,
            area: area,
            brush: brush
        };

        // Listener for window resize
        window.addEventListener('resize', () => this.resizeChart());

        // Return the chart with its elements
        return this.chart
    }

    /**
     * Updates some general widths and calls to update
     */
    resizeChart() {

        // Get new container width
        let width = $('.chart').width() - this.chart.dims.margin.left - this.chart.dims.margin.right;

        // Update width in chart and outer wrapper
        d3.select(".chart svg")
            .attr("width", width + this.chart.dims.margin.left + this.chart.dims.margin.right)
            .select("g")
            .attr("width", width);

        // Update domain range
        this.chart.domain.x.range([0, width]);

        // Update Y Axis lines
        this.chart.el.selectAll(".y.axis line")
            .attr("x2", width);

        // Call to chart update for the rest of elements
        this.updateChart(this.chart.datasets);
    }

    /**
     * Updates (Creates) chart elements with supplied data
     *
     * @param datasets
     * @returns {*}
     */
    updateChart(datasets = []) {

        // Get reference to chart elements
        let svg = this.chart.el,
            x = this.chart.domain.x,
            y = this.chart.domain.y,
            xAxis = this.chart.axis.x,
            yAxis = this.chart.axis.y,
            line = this.chart.line,
            area = this.chart.area,
            brush = this.chart.brush,
            aDatums = [];

        // Set dataset into chart for later use
        this.chart.datasets = datasets;

        // Concatenate different datasets for domains calculation
        datasets.forEach(function(entry) {
            aDatums = aDatums.concat(entry.data);
        });

        // Extend domains to fit datasets
        x.domain(brush.empty() ? d3.extent(aDatums, function(d) { return new Date(d[0]); }) : brush.extent());
        y.domain(d3.extent([[0,0]].concat(aDatums).concat([[0, 100]]), function(d) { return d[1]; }));

        // Reset brush if not yet
        if (!brush.empty()) brush.clear();

        // Update X Axis
        svg.select(".x.axis")
            .transition()
            .call(xAxis);

        // Update Y Axis
        svg.select(".y.axis")
            .transition()
            .call(yAxis);

        // Update brush
        svg.select(".brush")
            .call(brush);

        // Derive linear regressions
        let lrs = datasets.map(function(entry) {

            console.log(entry.metricsData.activeDatesRange);

            let ranges =  entry.metricsData.activeDatesRange,
                rangedData = entry.data.filter(d => {
                    let fromValid = !ranges.from || d[0] >= ranges.from,
                        toValid = !ranges.to || d[0] <= ranges.to;
                    return fromValid && toValid;
                });

            return entry.data && entry.data.length ?
                ss.linearRegressionLine(ss.linearRegression(rangedData)) :
                () => 0;
        });

        // Create lines based on the beginning and endpoints of the range
        let lrsData = lrs.map(function(entry) {
            return x.domain().map(function(x) {
                return [x.getTime() || 0, Math.max(entry(x), 0)]
            });
        });

        // Loop over datasets
        datasets.forEach(function(entry, i) {

            // Get title for this dataset
            let title = entry.title.toLowerCase().split(/ +/).join('_');

            // Draw lines: default and regression one
            [{data: entry.data, main: true}, {data: lrsData[i]}].forEach(draw => {

                // Build classes names and other init variables
                let lClass = (draw.main ? 'line' : 'reg') + '-' + title,
                    aClass = 'area-' + title,
                    eArea = null;

                // Check if main line
                if (draw.main) {

                    // Select area by class and assign data
                    eArea = svg.selectAll('.' + aClass)
                        .data(draw.data, d => aClass);
                }

                // Select line by class and assign data
                let eLine = svg.selectAll('.' + lClass)
                                .data(draw.data, d => lClass);

                // If got area, draw it first
                if (eArea) {

                    // Exit
                    eArea.exit()
                        .remove();

                    // Enter
                    eArea.enter()
                        .append("path")
                        .attr("class", 'area ' + aClass);

                    // Update
                    eArea.transition()
                        .attr("d", area(draw.data));
                }

                // Exit
                eLine.exit()
                    .remove();

                // Enter
                eLine.enter()
                    .append('path')
                    .attr("class", (draw.main ? 'line' : 'reg') + ' ' + lClass);

                // Update
                eLine.transition()
                    .attr("d", line(draw.data));
            });

            // Select circles by class and assign data
            let eCircles = svg.selectAll('.circle-' + title)
                .data(entry.data, d => 'circle-' + title + '_' + d[0]);

            // Exit
            eCircles.exit()
                .remove();

            // Enter
            eCircles.enter()
                .append('circle')
                .attr("class", 'circle circle-' + title)
                .attr("r", 3)

                // Hover listener
                .on('mouseover', function(d) {

                    // Increase circle radius
                    d3.select(this)
                        .attr("r", 10);

                    // Position tooltip
                    let tooltip = d3.select(".chart-tooltip")
                        .style("top", (d3.event.pageY - 10) + "px")
                        .style("left", (d3.event.pageX + 10) + "px")
                        .classed("hidden", false);

                    // Change tooltip date to that of point
                    tooltip
                        .select(".date")
                        .text(new Date(d[0]).toDateString());

                    // Change tooltip value to that of point
                    tooltip
                        .select(".value")
                        .text(Math.round(d[1]) + '%');
                })

                // Unhover listener
                .on('mouseout', function(d) {

                    // Restore circle radius
                    d3.select(this)
                        .attr("r", 3);

                    // Hide tooltip
                    d3.select(".chart-tooltip")
                        .classed("hidden", true);
                });

            // Update
            eCircles
                .attr("cx", d => x(new Date(d[0])))
                .attr("cy", d => y(+d[1]));
        });

        // Return chart with elements here too
        return this.chart;
    }
}
