import 'react'

import Navbar from './navbar.jsx';
import Content from './content.jsx';
import Loader from './loader.jsx';

export default class MainContainer extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            filters: props.filters || {},
            data: props.data || [],
            metricsData: props.metricsData || [],
            loading: props.loading
        };
    }

    componentWillReceiveProps(nextProps) {
        console.log('Main Container Will receive props ...', this, nextProps);
        this.setState({
            filters: nextProps.filters || this.state.filters,
            data: nextProps.data || this.state.data,
            metricsData: nextProps.metricsData || this.state.metricsData,
            loading: nextProps.loading
        });
    }

    shouldComponentUpdate(nextProps, nextState) {
        console.log('Should Main Container update?', nextProps, nextState);
        let b = nextState.filters &&
            (nextState.filters.chart && (this.state.filters.chart !== nextState.filters.chart) ||
            nextState.filters.store_id && (this.state.filters.store_id !== nextState.filters.store_id) ||
            nextState.filters.start && (this.state.filters.start !== nextState.filters.start) ||
            nextState.filters.end && (this.state.filters.end !== nextState.filters.end)) ||
            nextState.data && JSON.stringify(nextState.data) !== JSON.stringify(this.state.data) ||
            nextState.metricsData && JSON.stringify(nextState.metricsData) !== JSON.stringify(this.state.metricsData) ||
            nextState.loading !== this.state.loading;
        return !!b
    }

    componentWillUpdate(nextProps, nextState) {
        console.log('Main Container Will update ...', this, nextProps, nextState);
    }

    componentDidUpdate(prevProps, prevState) {
        console.log('Main Container Updated!!', this, prevProps, prevState);
    }

    render() {
        console.log('Rendering Main Container!!!');
        return (
            <div className="main-container">
                <Loader show={this.state.loading} />
                <Navbar router={this.props.router} filters={this.state.filters} />
                <Content data={this.state.data} metricsData={this.state.metricsData} filters={this.state.filters} />
            </div>
        )
    }
}

