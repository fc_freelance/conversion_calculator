import 'react'
import Chart from './chart.jsx';

export default class Content extends React.Component {

    constructor(props) {
        super(props);
    }

    componentWillMount() {
        //console.log('Content Will mount ...', this);
    }

    componentDidMount() {
        //console.log('Content Mounted!', this);
    }

    componentWillUnmount() {
        console.log('Content Will unmount ...', this);
    }

    componentWillReceiveProps(nextProps) {
        console.log('Content Will receive props ...', this, nextProps);
    }

    shouldComponentUpdate(nextProps, nextState) {
        console.log('Should Content update?', nextProps, nextState);
        let b = nextProps.data && JSON.stringify(nextProps.data) !== JSON.stringify(this.props.data) ||
            nextProps.metricsData && JSON.stringify(nextProps.metricsData) !== JSON.stringify(this.props.metricsData) ||
            (nextProps.filters && (nextProps.filters.chart && (this.props.filters.chart !== nextProps.filters.chart) ||
            nextProps.filters.start && (this.props.filters.start !== nextProps.filters.start) ||
            nextProps.filters.end && (this.props.filters.end !== nextProps.filters.end)));
        return !!b;
    }

    componentWillUpdate(nextProps, nextState) {
        console.log('Content Will update ...', this, nextProps, nextState);
    }

    componentDidUpdate(prevProps, prevState) {
        console.log('Content Updated!!', this, prevProps, prevState);
    }

    render() {
        console.log('Rendering Content!!!');
        return (
            <div className="container-fluid content">
                <Chart data={this.props.data} metricsData={this.props.metricsData} filters={this.props.filters} />
            </div>
        )
    }
}
