import Backbone from 'backbone'
import 'react'
import 'react-dom'

import MainContainer from '../components/main_container.jsx'

export default Backbone.View.extend({

    router: null,
    filters: null,
    rEl: null,
    campaignsMetricsCollection: null,

    events: {
        "click button": "click"
    },

    initialize(options = {}) {
        this.campaignsMetricsCollection = options.campaignsMetricsCollection || null;
        this.router = options.router || {};
        this.filters = options.filters || {};
        this.listenTo(this.collection, "sync reset", () => this.update({loading: this.collection.length}));
        this.listenTo(this.campaignsMetricsCollection, "sync reset", () => this.update({loading: false}));
        this.listenTo(this.collection, "error", () => this.collection.reset(null));
        this.listenTo(this.campaignsMetricsCollection, "error", () => this.campaignsMetricsCollection.reset(null));
    },

    update(data = {}) {
        console.log('Full Update!!!', data, this.collection);
        if (!this.rEl) return console.log('Main container not rendered yet!');
        this.filters = data.filters || this.filters;
        this.rEl.setState({
            filters: _.clone(this.filters),
            data: this.collection && this.collection.toJSON(),
            metricsData: this.campaignsMetricsCollection && this.campaignsMetricsCollection.toJSON(),
            loading: (typeof data.loading === 'undefined') ? true : !!data.loading
        });
    },

    render() {
        console.log('Full Rendering!!!');
        let filters = _.clone(this.filters),
            data = this.collection && this.collection.toJSON(),
            metricsData = this.campaignsMetricsCollection && this.campaignsMetricsCollection.toJSON()
        this.rEl = ReactDOM.render(<MainContainer router={this.router} filters={filters} data={data} metricsData={metricsData} loading="true" />, this.el)
    },

    click() {
        console.log('click', this);
    }
})
