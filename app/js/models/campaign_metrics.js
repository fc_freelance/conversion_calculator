import Backbone from 'backbone'

import {default as CampaignMetric} from '../models/campaign_metric'
import {default as CampaignMetrics} from '../collections/campaign_metrics'

export default Backbone.Model.extend({

    baseUrl: '<the endpoint here, either from input field or hardcoded if public>',

    defaults: {
        status: 0,
        campaign: null,
        totals: null,
        facts: null
    },

    sync: (method, model, options = {}) => {
        options.dataType = options.dataType || 'xml';
        options.store_id = options.store_id || '<some default store id>';
        model.url = model.baseUrl + '&store_id=' + options.store_id + '&campaign_id=' + model.get('id') + '&from=' + options.from + '&to=' + options.to;
        Backbone.Model.prototype.sync.call(model, method, model, options)
    },

    parse: (data, options) => {
        let facts = [];
        $(data).find('fact').map((i, entry) => facts.push(parseMetrics(entry)));
        return {
            status: $(data).find('status').text(),
            campaign: parseMetrics(data, 'campaign'),
            totals: parseMetrics(data, 'totals'),
            facts: new CampaignMetrics(facts)
        };
    }
});

function parseMetrics(data, key) {
    let entry = key ? $(data).find(key) : data,
        period = $(entry).find('period').text(),
        match = /(\d{4})(\d{2})(\d{2})/.exec(period),
        period_date = match && new Date(match[1], parseInt(match[2], 10) - 1, match[3]);
    return new CampaignMetric({
        period: period_date || 0,
        loads: parseInt($(entry).find('loads').text(), 10),
        likes: parseInt($(entry).find('likes').text(), 10),
        sales: parseInt($(entry).find('sales').text(), 10),
        order_total: parseFloat($(entry).find('order_total').text())
    })
}
