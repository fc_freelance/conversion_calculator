import Backbone from 'backbone'

export default Backbone.Model.extend({

    defaults: {
        date: 0,
        storeName: 'Unknown',
        campaignId: 0,
        campaignName: 'Unknown',
        browser: 'Unknown',
        os: 'Unknown',
        device: 'Unknown',
        wasConversion: false,
        wasCampaign: false,
        wasInteracted: false,
        location: 'Unknown'
    }
})
