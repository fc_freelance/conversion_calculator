import Backbone from 'backbone'

export default Backbone.Model.extend({

    defaults: {
        period: null,
        loads: 0,
        likes: 0,
        sales: 0,
        order_total: 0
    }
})
